import { Request, Response } from "express";
import { SystemService } from "../services/system-service";

async function getInfo(req: Request, res: Response) {
    const service = new SystemService();
    res.json(service.getInfo());
}

export {
    getInfo,
};
