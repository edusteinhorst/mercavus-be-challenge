import { Request, Response } from "express";
import { UserService } from "../services/user-service";
import { NotFoundError } from "../errors/not-found-error";

async function createUser(req: Request, res: Response) {
    const service = new UserService();
    res.status(201);
    res.json(await service.createUser(req.body));
}

async function getUser(req: any, res: Response) {
    const service = new UserService();
    const user = await service.getUser(req.pathParams.id);
    return res.json(user);
}

async function listUsers(req: any, res: Response) {
    const { skip, limit } = req.query;
    const service = new UserService();
    res.json(await service.listUsers(skip, limit));
}

export {
    createUser,
    getUser,
    listUsers,
};
