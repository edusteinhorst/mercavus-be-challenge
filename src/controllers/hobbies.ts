import { Response } from "express";
import { HobbyService } from "../services/hobby-service";
import { INewHobby } from "../models/hobby";

async function createHobby(req: any, res: Response) {
    const service = new HobbyService();
    const hobby: INewHobby = {
        passionLevel: req.body.passionLevel,
        name: req.body.name,
        year: req.body.year,
        user: req.pathParams.userId,
    };
    res.status(201);
    res.json(await service.createHobby(hobby));
}

async function listHobbies(req: any, res: Response) {
    const { skip, limit } = req.query;
    const service = new HobbyService();
    res.json(await service.listHobbies(req.pathParams.userId, skip, limit));
}

async function deleteHobby(req: any, res: Response) {
    const service = new HobbyService();
    res.status(200);
    res.json(await service.deleteHobby(req.pathParams.userId, req.pathParams.id));
}

export {
    createHobby,
    listHobbies,
    deleteHobby,
};
