import { Request, Response } from "express";
import { ValidationError } from "../errors/validation-error";
import { ConflictError } from "../errors/conflict-error";
import { NotFoundError } from "../errors/not-found-error";
import logger from "../utils/logger";

export const handle = async (operationId: string, req: Request, res: Response) => {
    // swagger 'operationId' maps to controller.function
    logger.debug(`Serving operation ${operationId}.`);
    const controllerName = `./${operationId.split('.')[0]}`;
    const functionName = operationId.split('.')[1];
    const controller = await import(controllerName);
    if (!controller || !controller[functionName]) {
        throw new Error(`No controller/function found for operationId: ${operationId}`);
    }
    try {
        await controller[functionName](req, res);
    } catch (err) {
        // status code mapping based on https://tools.ietf.org/html/rfc7231
        if (err instanceof NotFoundError) {
            res.status(404);
            return res.json({
                statusCode: 404,
                detail: err.message,
            });
        }
        if (err instanceof ValidationError) {
            res.status(400);
            return res.json({
                statusCode: 400,
                detail: err.message,
            });
        }
        if (err instanceof ConflictError) {
            res.status(409);
            return res.json({
                statusCode: 409,
                detail: err.message,
            });
        }
        throw err;
    }
};
