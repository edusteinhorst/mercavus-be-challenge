import http from "http";
import express from "express";
import { Request, Response, NextFunction } from "express";
import createMiddleware from "swagger-express-middleware";
import swaggerUi from "swagger-ui-express";
import db from "./config/db";
import { handle } from "./controllers";
import logger from "./utils/logger";
import swaggerDocument from "./config/swagger.json";

const app = express();
createMiddleware(swaggerDocument, app, (err, middleware) => {
    app.use(
        middleware.metadata(),
        middleware.CORS(),
        middleware.files(),
        middleware.parseRequest(),
        middleware.validateRequest(),
    );
    app.use((req: any, res: Response, next: NextFunction) => {
        const opId = req.swagger.operation.operationId;
        handle(opId, req, res).catch((error) => {
            logger.error('Controller failure: ', error);
            next({
                status: 500,
                message: 'Internal error.',
            });
        });
    });
    app.use((error: any, req: Request, res: Response, next: NextFunction) => {
        res.status(error.status);
        res.json({
            status: error.status,
            detail: error.message,
        });
    });
});

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

const { PORT = 3000 } = process.env;
const httpServer = http.createServer(app);

httpServer.listen(PORT, async () => {
    await db.init();
    httpServer.on("appStopped", async () => {
        await db.stop();
    });
    logger.info(`Server is running http://localhost:${PORT}...`);
    httpServer.emit("appStarted");
});

export const server = httpServer;
