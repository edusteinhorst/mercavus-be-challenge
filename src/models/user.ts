import mongoose, { Schema, Document } from 'mongoose';
import { IHobby } from './hobby';

const userSchema: Schema = new Schema({
    name: { type: String, required: true },
});

userSchema.set("toJSON", {
    transform: (doc, ret, options) => {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
    },
});

export interface INewUser {
    name: string;
}

export interface IUser extends Document {
    id: string;
    name: string;
    hobbies: IHobby[];
}

export default mongoose.model<IUser>('User', userSchema);
