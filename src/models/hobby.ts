import mongoose, { Schema, Document } from 'mongoose';
import { IUser } from './user';
import { ObjectId } from 'bson';

const hobbySchema: Schema = new Schema({
    passionLevel: { type: String, enum: ['low', 'medium', 'high', 'very-high'], required: true },
    name: { type: String, required: true },
    year: { type: Number, required: true },
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
});

hobbySchema.set("toJSON", {
    transform: (doc, ret, options) => {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
    },
});

hobbySchema.index({ name: 1, user: 1 }, { unique: true });

enum PassionLevel {
    Low = "low",
    Medium = "medium",
    High = "high",
    VeryHigh = "very-high",
}

export interface INewHobby {
    passionLevel: PassionLevel;
    name: string;
    year: number;
    user: string;
}

export interface IHobby extends Document {
    id: string;
    passionLevel: PassionLevel;
    name: string;
    year: number;
    user: ObjectId;
}

export default mongoose.model<IHobby>('Hobby', hobbySchema);
