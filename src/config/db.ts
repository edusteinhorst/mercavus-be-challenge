import mongoose, { ConnectionOptions } from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';
import logger from "../utils/logger";

let mongoServer: MongoMemoryServer;

const options: ConnectionOptions = {
    keepAlive: true,
    autoReconnect: true,
    poolSize: Number.parseInt(process.env.DB_POOL_SIZE || '5', 10),
    connectTimeoutMS: 30000,
    socketTimeoutMS: 30000,
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 1000,
    useFindAndModify: false,
    useNewUrlParser: true,
    useCreateIndex: true,
};

export = {
    init: async () => {
        let connectionString = process.env.DB_URL || "localhost";
        if (process.env.MONGO_IN_MEMORY && process.env.MONGO_IN_MEMORY === 'true') {
            mongoServer = new MongoMemoryServer();
            connectionString = await mongoServer.getConnectionString();
            logger.warn(`Using in-memory MongoDB.\n`);
        }
        await mongoose.connect(connectionString, options);
        logger.info(`Mongoose set with ${options.poolSize} connections.`);
    },
    stop: async () => {
        await mongoose.disconnect();
        if (process.env.MONGO_IN_MEMORY) {
            await mongoServer.stop();
        }
    },
};
