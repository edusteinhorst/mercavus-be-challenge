import request from "supertest";
import { server } from "../server";
import * as chai from "chai";
import { IUser } from "../models/user";

const expect = chai.expect;

describe('Integration - /users', () => {
    let user: IUser;
    it('should create a user', async () => {
        const newUser = {
            name: "Dummy name",
        };
        user = (await request(server)
            .post('/users')
            .send(newUser)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(201)).body;
        expect(user).to.have.property('id');
        expect(user.name).eq(newUser.name);
    });
    it('should get user', async () => {
        const found: IUser = (await request(server)
            .get(`/users/${user.id}`)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)).body;
        expect(found).eql(user);
    });
    it('should list users', async () => {
        const users = (await request(server)
            .get('/users')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)).body;
        expect(users).to.include.deep.members([user]);
    });
});
