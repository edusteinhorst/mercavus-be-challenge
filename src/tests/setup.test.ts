import { server } from "../server";
import logger from "../utils/logger";

before((done) => {
    logger.setLevel('warn');
    server.on('appStarted', () => {
        done();
    });
});

after(() => {
    server.close();
    server.emit("appStopped");
});
