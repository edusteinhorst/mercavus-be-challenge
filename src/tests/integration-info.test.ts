import request from "supertest";
import { server } from "../server";
import packageInfo from "../../package.json";

describe('Integration - /info', () => {
    it('should get system info', async () => {
        await request(server)
            .get('/info')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, { name: packageInfo.name, version: packageInfo.version });
    });
});
