import packageInfo from "../../package.json";

export class SystemService {
    public getInfo() {
        return {
            version: packageInfo.version,
            name: packageInfo.name,
        };
    }
}
