import Hobby, { IHobby, INewHobby } from "../models/hobby";
import { UserService } from "./user-service";
import { ConflictError } from "../errors/conflict-error";
import { ObjectId } from "bson";
import { NotFoundError } from "../errors/not-found-error";

export class HobbyService {
    public async createHobby(input: INewHobby): Promise<IHobby> {
        const userService = new UserService();
        await userService.getUser(input.user);
        try {
            const hobby: IHobby = new Hobby(input);
            return await hobby.save();
        } catch (err) {
            if (err.code && err.code === 11000) {
                throw new ConflictError("Hobby already exists for user.");
            }
            throw err;
        }
    }
    public async listHobbies(userId: string, skip: number = 0, limit: number = 200): Promise<IHobby[]> {
        const userService = new UserService();
        await userService.getUser(userId);
        return Hobby.find({ user: userId }, { id: 1, passionLevel: 1, name: 1, year: 1 }).skip(skip).limit(limit);
    }
    public async getHobby(userId: string, id: string): Promise<IHobby> {
        const userService = new UserService();
        await userService.getUser(userId);
        if (!ObjectId.isValid(id)) {
            throw new NotFoundError("Hobby not found.");
        }
        const hobby = await Hobby.findById(id);
        if (!hobby || hobby.user.toHexString() !== userId) {
            throw new NotFoundError("Hobby not found.");
        }
        return hobby;
    }
    public async deleteHobby(userId: string, id: string) {
        const hobby = await this.getHobby(userId, id);
        return hobby.remove();
    }
}
