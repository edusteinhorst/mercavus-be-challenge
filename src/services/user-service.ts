import User, { IUser, INewUser } from "../models/user";
import { ObjectId } from "bson";
import { NotFoundError } from "../errors/not-found-error";

export class UserService {
    public async createUser(input: INewUser): Promise<IUser> {
        const user: IUser = new User(input);
        return user.save();
    }
    public async getUser(userId: string): Promise<IUser> {
        if (!ObjectId.isValid(userId)) {
            throw new NotFoundError("User not found.");
        }
        const user = await User.findById(userId);
        if (!user) {
            throw new NotFoundError("User not found.");
        }
        return user;
    }
    public async listUsers(skip: number = 0, limit: number = 200): Promise<IUser[]> {
        return User.find().skip(skip).limit(limit);
    }
}
