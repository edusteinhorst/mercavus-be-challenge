import "mocha";
import * as chai from "chai";
import { SystemService } from "./system-service";
import packageInfo from "../../package.json";

const expect = chai.expect;

describe('Unit - SystemService', () => {
    it('should get system info', async () => {
        const service = new SystemService();
        const info = service.getInfo();
        expect(info.name).equals(packageInfo.name);
        expect(info.version).equals(packageInfo.version);
    });
});
