import { createLogger, format, transports } from "winston";

const logger = createLogger({
    transports: [
        new transports.Console({
            format: format.combine(
                format.colorize(),
                // format.timestamp({ format: 'HH:mm:ss' }),
                format.simple(),
            ),
        }),
    ],
    exitOnError: false,
});

export default {
    debug: (message: string, error?: Error) => {
        logger.debug(message, error);
    },
    info: (message: string, error?: Error) => {
        logger.info(message, error);
    },
    warn: (message: string, error?: Error) => {
        logger.warn(message, error);
    },
    error: (message: string, error?: Error) => {
        logger.error(message, error);
    },
    setLevel: (level: string) => {
        logger.level = level;
    },
};
