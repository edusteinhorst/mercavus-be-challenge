[![pipeline status](https://gitlab.com/edusteinhorst/mercavus-be-challenge/badges/master/pipeline.svg)](https://gitlab.com/edusteinhorst/mercavus-be-challenge/badges/commits/master)
[![coverage report](https://gitlab.com/edusteinhorst/mercavus-be-challenge/badges/master/coverage.svg)](https://gitlab.com/edusteinhorst/mercavus-be-challenge/badges/commits/master)

# Mercavus - Backend Challenge

This service is designed simulate a simple REST API for users and their hobbies.

The API documentation is available at http://localhost:3000/api-docs once the service is running.

This project is part of a take-home assignment for Mercavus GmbH.

## Getting Started

### Prerequisites

There is no particular pre-requisite to run the service. 

For testing/development purposes, this project uses an in-memory MongoDB server. For production use a MongoDB URL can be specified in the file `config/env.json`.

To test the API, you can use the API documentation on your web browser or your favorite REST client such as [Insomnia](https://insomnia.rest).

### Setup and Running

#### Launching

Install npm dependencies:

```
npm install
```

Launch in development mode (hot reload):

```
npm run dev
```

Launch in production mode (need external database):

```
npm run prod
```

### Usage

The API documentation is available at http://localhost:3000/api-docs.

## Development

### Tests

Tests are coded using [Mocha](https://mochajs.org) and [Chai](https://www.chaijs.com), [SuperTest](https://github.com/visionmedia/supertest) for integration testing and [Instambul](https://istanbul.js.org) for code coverage. Tests can be run locally with:

```
npm test
```

### Coding Style

The code is writen in TypeScript and linted with [TSLint](https://palantir.github.io/tslint/).

### Built With

* [Express](http://expressjs.com) - Web framework
* [swagger-express-middleware](https://apitools.dev/swagger-express-middleware/) - Express middleware for Swagger validation
* [Mongoose](https://mongoosejs.com) - ODM framework for MongoDB
* [mongodb-memory-server](https://github.com/nodkz/mongodb-memory-server) - MongoDB memory server
* [Winston](https://github.com/winstonjs/winston) - Logger library
* [env-cmd](https://github.com/toddbluhm/env-cmd) - Environment files library
* [TSLint](https://palantir.github.io/tslint/) - Code linting library

## Future Enhancements

### Containerization

This project can be easily dockerized, which allows for better horizontal scaling and processes management. For this reason, a process manager such was PM2 was not included.
