#!/bin/bash
docker ps -a | grep -q mercavus-db || docker run --name mercavus-db -p 27017:27017 -d mongo:4.1.8-xenial
docker start mercavus-db
